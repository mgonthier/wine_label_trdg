import os

import augly.image as imaugs
import numpy as np

# PATHS
SRC_DIR = os.getcwd()
VOCAB_PATH = os.path.join(SRC_DIR, 'data', 'vocab')
IMAGE_OUT_DIR = os.path.join(SRC_DIR, 'output', 'images')
ANNOTATIONS_PATH = os.path.join(SRC_DIR, 'output', 'train_label.txt')
FONT_DIR = os.path.join(SRC_DIR, 'data', 'fonts')
FONTS = [os.path.join(FONT_DIR, font) for font in os.listdir(FONT_DIR)]

# TRDG PARAMETERS
BACKGROUND_TYPE = 3
DATASET_SIZE = 100
IMAGE_SIZE = 128
MARGINS = (10, 10, 10, 10)
TEXT_COLOR = '#000000,#999999'

# AUGLY IMAGE AUGMENTATION PARAMETERS
BLUR_RADIUS_RANGE = np.linspace(0.0001, 3, 10)
BRIGHTNESS_FACTOR_RANGE = np.linspace(0.25, 2, 10)
CONTRAST_FACTOR_RANGE = np.linspace(0.5, 1.5, 10)
PERSPECTIVE_TRANSFORM_SIGMA_RANGE = np.linspace(0, 30, 31)
SATURATION_FACTOR_RANGE = np.linspace(0.5, 1.5, 10)

# LIST OF AUGLY IMAGE AUGMENTATIONS
AUGLY_AUGMENTATIONS = [
    imaugs.OneOf(
        [
            imaugs.Blur(radius=radius)
            for radius in BLUR_RADIUS_RANGE
        ]
    ),
    imaugs.OneOf(
        [
            imaugs.Brightness(factor=factor)
            for factor in BRIGHTNESS_FACTOR_RANGE
        ]
    ),
    imaugs.OneOf(
        [
            imaugs.Contrast(factor=factor)
            for factor in CONTRAST_FACTOR_RANGE
        ]
    ),
    imaugs.OneOf(
        [
            imaugs.PerspectiveTransform(sigma=sigma)
            for sigma in PERSPECTIVE_TRANSFORM_SIGMA_RANGE
        ]
    )
]
AUGLY_TRANSFORMS = imaugs.Compose(AUGLY_AUGMENTATIONS)

# OTHER GENERATOR PARAMETER
VERTICAL_PROB = 0.1
CONVERT_VOCAB_TO_ASCII = True

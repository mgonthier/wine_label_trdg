import os
import pickle

import pandas as pd

root_dir = os.getcwd()
product_data_path = os.path.join(root_dir, 'data', 'product_df')
vocab_out_path = os.path.join(root_dir, 'data', 'vocab')


def main():
    df = load_raw_data()
    vocab = extract_vocab(df)
    export_vocab(vocab)


def load_raw_data():
    with open('./data/product_df', 'rb') as file:
        product_df = pickle.load(file)

    return product_df


def extract_vocab(raw_data):
    vocab_columns = [
        'appellation',
        'classification',
        'designation',
        'country',
        'region_1',
        'region_2',
        'producer'
    ]
    vocab_columns.extend([col for col in raw_data.columns if 'grape' in col])
    raw_vocab = [
        word
        for word_list in [
            extract_words_from_column(raw_data[col]) for col in vocab_columns
        ]
        for word in word_list
    ]
    vocab = clean_vocab(raw_vocab)

    return vocab


def extract_words_from_column(column):
    split_values = [val.split(' ') for val in list(column)]
    word_list = [word for split in split_values for word in split]

    return word_list


def clean_vocab(raw_vocab):
    vocab_no_missing = remove_missing_values(raw_vocab)
    vocab = list(set(vocab_no_missing))

    return vocab


def remove_missing_values(vocab):
    vocab_no_missing = [
        word for word in vocab if (
            pd.notnull(word) &
            (
                word not in [
                    '',
                    "Ne s'applique pas",
                    'Not applicable',
                    'Producteur à définir',
                    'To define'
                ]
            )
        )
    ]

    return vocab_no_missing


def export_vocab(vocab):
    with open(vocab_out_path, 'wb') as file:
        pickle.dump(vocab, file)


if __name__ == '__main__':
    main()

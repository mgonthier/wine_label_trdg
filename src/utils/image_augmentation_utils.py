import random

import numpy as np
import numpy.ma as ma
from scipy import stats


def fill_background(img: np.ndarray) -> np.ndarray:
    background_color = get_background_color(img)
    mask = ma.masked_not_equal(img, np.array([0, 0, 0])).mask
    filled_img = np.where(mask, img, background_color).astype(np.uint8)

    return filled_img


def get_background_color(img: np.ndarray) -> np.ndarray:
    color_array = get_color_array(img)
    non_black_colors = [col for col in color_array.reshape(-1) if col != '000#000#000']
    color_mode = stats.mode(non_black_colors).mode[0]
    color_mode_array = np.array([int(x) for x in color_mode.split('#')])

    return color_mode_array


def get_color_array(img: np.ndarray) -> np.ndarray:
    color_array = np.apply_along_axis(
        lambda x: '#'.join([f'{y:03}' for y in x]),
        axis=2,
        arr=img
    )

    return color_array


def random_rotation_to_vertical(img: np.ndarray, prob: float) -> np.ndarray:
    if random.uniform(0, 1) <= prob:
        n_rotations = random.choice([1, 3])
        img = np.rot90(img, k=n_rotations)

    return img

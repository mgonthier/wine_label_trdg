import pickle

from generator_config import (
    ANNOTATIONS_PATH,
    BACKGROUND_TYPE,
    CONVERT_VOCAB_TO_ASCII,
    DATASET_SIZE,
    FONTS,
    IMAGE_OUT_DIR,
    IMAGE_SIZE,
    MARGINS,
    TEXT_COLOR,
    AUGLY_TRANSFORMS,
    VERTICAL_PROB,
    VOCAB_PATH
)
from src.generator.text_image_generator import TextImageGenerator


def load_vocabulary():
    with open(VOCAB_PATH, 'rb') as file:
        vocabulary = pickle.load(file)

    return vocabulary


if __name__ == '__main__':
    vocab = load_vocabulary()
    generator = TextImageGenerator(
        vocab=vocab,
        dataset_size=DATASET_SIZE,
        fonts=FONTS,
        image_size=IMAGE_SIZE,
        text_color=TEXT_COLOR,
        background_type=BACKGROUND_TYPE,
        margins=MARGINS,
        augly_transforms=AUGLY_TRANSFORMS,
        vertical_prob=VERTICAL_PROB,
        convert_to_ascii=CONVERT_VOCAB_TO_ASCII
    )
    generator.generate_images(IMAGE_OUT_DIR, ANNOTATIONS_PATH)

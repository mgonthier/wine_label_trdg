import os
import random

import numpy as np
from PIL import Image
from trdg.generators import GeneratorFromStrings
from unidecode import unidecode

from src.utils.image_augmentation_utils import (
    fill_background,
    random_rotation_to_vertical
)


class TextImageGenerator:

    def __init__(
            self,
            vocab: list,
            dataset_size: int,
            fonts: list,
            image_size: int,
            text_color: str,
            background_type: int,
            margins: tuple,
            augly_transforms,
            vertical_prob: float = 0,
            convert_to_ascii: bool = True
    ):
        if convert_to_ascii:
            self.vocab = [unidecode(word) for word in vocab]
        else:
            self.vocab = vocab
        self.dataset_size = dataset_size
        self.fonts = fonts
        self.image_size = image_size
        self.text_color = text_color
        self.background_type = background_type
        self.margins = margins
        self.generator = GeneratorFromStrings(
            self.vocab,
            count=dataset_size,
            fonts=fonts,
            word_split=True,
            size=image_size,
            text_color=text_color,
            background_type=background_type,
            margins=margins
        )
        self.augly_transforms = augly_transforms
        self.vertical_prob = vertical_prob

    def generate_images(self, image_out_dir, annotations_path):
        if not os.path.exists(image_out_dir):
            os.makedirs(image_out_dir)
        with open(annotations_path, 'w') as annotations_file:
            for i, (image, label) in enumerate(self.generator):
                annotation_line = f'images/{i}.jpg {label}\n'
                annotations_file.write(annotation_line)
                augmented_image = self.augment_image(image)
                augmented_image.save(os.path.join(image_out_dir, f'{i}.jpg'))

    def augment_image(self, image):
        if self.augly_transforms:
            image = self.augly_transforms(image)
            image_array = np.array(image)
            image_array = fill_background(image_array)
        else:
            image_array = np.array(image)
        image_array = random_rotation_to_vertical(image_array, self.vertical_prob)
        augmented_image = Image.fromarray(image_array)

        return augmented_image
